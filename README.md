# SRTD_ISRO
Planetary Visualizations and Analysis Portal
MOON | MARS | VENUS | EARTH | SUN | Space Station | Any Other Planetary Body

REQUIRED FEATURES:
⦁	2.5D using vector and raster data info on planet scale
	most suitable library for any 2.5D/3D is cesiumJS, next is terriaJS 
⦁	Coordinate reference system / Map Projections support as per planetary body
	cesium supports this
⦁	Stereo/Anaglyph data i.e. 3D
⦁	Vector data support (geojson, shp etc) 
	shp file can be converted to geojson file by QGIS and the geojson file can be 	put to geoserver and canbe used as a layer in cesium and terria.
⦁	major format support (Geotiff, PNG, jpeg, giff)
⦁	Video file supported along with coordinates
⦁	3D model support along with coordinates
	terriaJS supports this 
⦁	Query terrain for height and other important parameters
⦁	Measurement on the terrain (height, width, length, area)
	data can be embedded, cesium and terria supports it idk about others
⦁	Scene animation based on viewpoint
⦁	Detailed information of feature in pop up
⦁	Data styling (color, line, points etc) using varies conditions/expressions
	can be done in cesium and terria
⦁	Clustering based on data (no. of craters, no. of missions etc.)
	data can be embedded 	with places in cesium and a simple search algo can 	do the job
⦁	Support for thematic layers (Based on mineral, chemical and other parameters)
	cesium and terria has scope
⦁	Diaplay scale of the map/zoom and controls/compass
	It is there in almost every library including cesium and terria
⦁	Support to add live data (live operations)
⦁	Query place name and fly to that place
	cesium and terria support the searchoption which can be easily implemented 	if we have the required data on the geoserver
⦁	Placement of label/icon
⦁	Colonization on other planets (supports adding building/dome in 3D)
	cesium supports it, we can make shp file of the additional building and 	convert the file to geojson using QGIS and then we can put it as a layer 
⦁	Local language display support for the content (Hindi, gujrati etc.)
⦁	Android and IOS support
	android will support the website and it should be supported by IOS too
⦁	VR/AR support for landing simulations/rover movements/moon walk etc.
⦁	Support for deep learning model incubation for analysis (clasification, object identification, analytical models)
	In cesium and terria, we can implement this where the voice command is used to type using the inbuilt NLP model and after that the input can be sent to the LLM model to search for answer which can be queried to  search and the place can be located
⦁	Data analysis and image processing
		- visual and quantitative analysis of two image from two times 
			terriaJS supports this
		- Swipe between images, Data Cube (switching the images as per the 			 	   viewer's position)
		- Time series analysis of other datasets
		- Placement of appropriate rich text labels/legends on data
		- Add a new marker/point/image
		- Filter data as per the features/query
			terriaJS supports this
		- Application of deep learning model for live analysis
		- Important GIS operations on hosted data
		- Creating a new layer based on analysis or adding the layer
		- Display data base on expression
		- Data highlighting based on the query/events
		- Base imgae processing operations (merging bands, denoising, super 			    	   resolution etc.)
⦁	Support of chat bot which support query in local language
	possible in cesium and terria
⦁	Simulations based on sun shadow, solar eclipse, asteroid, wind vector wtc
⦁	Navigational support for rover on moon, mars, venus (AR/VR)
⦁	Animate gravity and physics based orbit/landing simulations
⦁	Visualizations of sensor footprint based on trajectory

___________________________________________________________________________

Frameworks that can be considered - To explore:
- TerriaJS - an upgrade on cesiumJS, terria has more features but cesium is easy to build
- CesiumJS
- Blend4Web
- Leaflet (ReactLeaflet)
- Unity Webgl (good for AR/VR)
- OpenLayer3
- MapLibreJS
- OpenC graph
- OSg graph

Other terms encountered during surfing:
- D3.js
- GDAL
- Leaflet
- ArcGIS - NO, it is for mapping, not so suitable for 3D

Things visioned to have in the site:
- 2.5D to 3D dipiction
- Support AR/VR 
- Geospatial Data Analysis features
- Deep Learning / AI model can be implemented (LLM/NLP models for voice search)
- Planetary reference systems should be proper
- Live Data input should be supported 

To learn:
- Geospatial data
- DEM/IMAGE files (learn and explore)
- Tiling/Level of details - Done for Map server / Geoserver
- Vector Data (learn and explore)

___________________________________________________________________________

Geospatial Data 
- GIS technology is used for processing geo spatial data (mapping or analysing)
- 4 main ideas of GIS:
	- Create geographic data
	- Manage it in database
	- Analyse and find patterns - Geo Processing
	- Display it on map
- 2 types of data that the GIS management system deals with:
	- Vector Data - Points, Lines and Polygons with vertices
	- Raster Data/ Grid Data (represented as rows and columns) - DEM (Digital Elevation 	Model), Satellite images and Aerial photo

___________________________________________________________________________

LIBRARY	2.5D/3D	AR/VR		ANALYSIS	PLANETARY REF SYS	
TerriaJS		    Y		    N (plugin)	      Y			Y
CesiumJS	    Y		    Y (N)		      Y			Y
Blend4Web	    Y		    Y		Limited			N
Leaflet	                  Y (N)		    N (plugin)	Limited			N
Unity WebGL	    Y		    Y		Limited			N
Open Layer 3	    Y		    N		      Y			N
MapLibre JS	    Y (3D-N)	    N		Limited			Y

MapLibre - Has a community and tutorials. Not suitable for 3D 
CesiumJS - Has larger community and tutorials. Most suitable for 3D
TerriaJS - Most suitable for data analysis - more tools for data visualization as compared to caesium. Uses cesium and WebGL for a full 3D globe in browser and falls back to 2D with leaflet on systems that can't run cesium.

For 2D:
MapboxGL - Most feature loaded and has the most options and flexibility when 2D is involved


___________________________________________________________________________

SOME IMPORTANT PLUGS
planetarypixelemporium.com -> has texture maps for different planets which we can download
tomcat.apache.org -> web server commonly used for running cesium inside it for trial
youtube.com/watch?v=wDPY3lRfhVo -> beginners go-through for cesium capabilities and functionalities that can be implemented using it.
QGIS -> a free software available convert vector data to geojson data which will be used to overlay 

___________________________________________________________________________

nationalmap.gov.au -> A site for data analysis of australia by australian government which is based on terriaJS and has the features that we would want in our site. 
github.com/TerriaJS/terriajs/blob/main/README.md -> refer it for terriajs applications in real world applications till now

___________________________________________________________________________

To Do Today (15/5):
- terriaJS and openGlobus -> to explore its working and whether it is paid or not
- know the full working of geoserver

___________________________________________________________________________

16/5

- continuing to study geoserver and GIS and exploring techstacks used in other such portals
BHUVAN
- Bhuvan is slow in running queries and the 3D visualizations are not crisp
- Techstack used are:
	- OpenLayers API - client side scripting language
	- Geoserver, Geowebcache - for vector data serving
	- UMN Mapserver, TileCache - for raster data serving
	- PHP - server side scripting language
	- JASIG - central authentication service
	- PostgreSQL, PostGIS - RDBMS
	- Apache/ Tomcat - serving the web pages
	- Javascript, JQuery, VueJS - Client side navigations and validations

NATIONALMAP
- A lot faster and better than Bhuvan
- TerriaJS - relies heavily on it for client side
- There are several data contributors (government organisations generally) and hosting providers who utilizes their own technologies for serving data so the server and database management is not done by the site managers. NationalMap harvest data from external sources and does not store it internally. It doesn't utilize a traditional DBMS. The contributors may have their own DBMS technologies.

-> Why Bhuvan didn't utilize the TerriaJS but rather planned to go for OpenLayersAPI? Because Bhuvan is not fast enough like NationalMap!
	- Because Bhuvan was made in 2009 and nationalmap was made in 2014. the 	techstack "TerriaJS" was not present with the developers of Bhuvan when it was 	being made so they used the already present open source stacks and cesium(2012) 	was not open sourced till then. 
	- Also, Bhuvan has its own DBMS and nationalMap is having data contributors and 	thus the data serving and fetching is a bit fast becuase load on database and server 	is less and their TerriaJS is a lot more optimised and compatible than OpenLayers for 	large data.


History of TerriaJS:
- NationalMap was developed by two organisations - NICTA and CSIRO Digital Productivity Flagship, in 2014. The two organisations integrated and formed Data61 in 2016. The development of nationalMap was done on a framework that was based on two open source frameworks - Cesium and leaflet. This framework was TerriaJS which as of now is the most compatible library for 3D planetary visualizations supporting geo-spatial data analysis at the same time, according to me. Since the development of nationalMap, TerriaJS was put on open source. Data61 is handling it as of now and the repo is open for contributions

- The use of TerriaJS is seen in nationalmap and there is no use of it in any other such national large scale site by other countries since 2014 when nationalmap.gov.au was deployed or else I don't have enough data yet. 

___________________________________________________________________________

Proposed Tech-Stack for the portal:
⦁	TerriaJS (Client side data 3D visualization and data analysis)
⦁	ReactJS (Client side), NodeJS (Server side)
⦁	Geoserver , Geowebcache (Data serving and tiling solution)
⦁	PostgreSQL, PostGIS (RDBMS)
⦁	Apache Tomcat (Application server for serving web page)

___________________________________________________________________________

17/05
- openGlobus -> explore
- try to deploying a terriaJS application -> docs-v7.terria.io/guide/getting-started/#prerequisites -> do “yarn install” and then “yarn gulp dev” to deploy the map

Deployed geoserver in windows binary platform-independent
Next day - find a way to integrate geoserver to terriaJS and explore the terriaJS code base to find how to link my own data server to it and update UI of teriaJS.

___________________________________________________________________________

20/05
-> There is an issue of compatibility with all other versions of JDK, use JDK 11 strictly for local server deployment. Other versions are able to host geoserver but all the functionality are not present, like the layer preview by openLayers which was showing a HTTP ERROR 500 (internal web server error) and it is not showing in JDK 11 version. 

-> Also which installing JDK 11, the folder “eclipse adoptium" created should be given all the permisions necessary otherwise the geoserver will deploy but it will show a HTTP ERROR 500 which is a problem with Java AWT on a headless server environment

-> geoserver download folder in C drive should be given the required permissions 

___________________________________________________________________________

20/05 - 24/05
-> The ISRO restriction on the internet and system downloads posed problem in these days while the holiday was on 23rd. 
-> Figured out that the "TerriaMap" repository is not the main one which we should be editing for the changes in the site, rather the repo to be considered for this is "TerriaJS" and here we can find everything and most of it is customizable by editing it. 
-> Next focus is changing the logos and loaders and basic UI relating to transparency and color palette of the page. 
-> Then move to integrating geoserver with TerriaJS for data serving
-> Also have to explore cesiumJS and deploy a simple visualization and data analysis model, use the following tutorial -> https://www.youtube.com/watch?v=wDPY3lRfhVo&t=177s

___________________________________________________________________________

27/05
-> We can copy the wwwroot directory of a working TerriaJS application up to whatever web server runs your site and it will run there just as well as it does on the Node-based server. We'll have to make sure that all of your geospatial data is either on the same server or is hosted on servers that support CORS, and shapefile conversion won't be supported, but other than that we should be good to go.
-> Go to TerriaJS-main/doc/contributing/frontend-style-guide.md for majority of frontend

___________________________________________________________________________

29/05/2024 [WFH]
I git cloned both the terriaJS and terriaJS-server repositories in a folder, I did the “docker run -d -p 3001:3001 ghcr.io/terriajs/terriamap” to run it. Had to set up Docker in system. 
3D terria in port 3001 cannot be rendered because it needs connection to terriaJS-server and I need to figure out how to connect the two
Furthermore, I need to figure out how to connect geoserver to terrisJS

___________________________________________________________________________

30/05/2024 – 31/05/24 [WFH]
⦁	F terriajs-server, there is a process to connect the terriaJS to geoserver and feed the dtat directly to it, but the documentation was for ubuntu, so had to figure out everything by trial and run basis of my interpretation of doc for windows OS
⦁	Installed JDK 11
⦁	Set env variables for JAVA_HOME and update the path variable for %JAVA_HOME%\bin
⦁	Installed Apache Tomcat
⦁	Set env variable for CATALINE_HOME
⦁	Installed Geoserver
⦁	Copied “geoserver.war” file to tomcat “webapps” directory
⦁	Started geoserver
⦁	Enabled CORS -> downloaded cors filter library -> added .jar file in tomcat lib directory -> uncommented some CORS code in web.xml file in Tomcat\webapps\geoserver\WEB-INF -> start tomcat by webapps\bin\startup.bat
⦁	Make a layer in geoserver by appropriate process (This needs to be done)
⦁	Download a shp file and place it in this location: 
“Tomcat9.0\webapps\geoserver\data\data\shapefiles”
⦁	Deploy the apache tomcat by startup.bat and go to this url - http://localhost:8082/geoserver, then select the workspaces option and make a workspace “New_workspace” then come to store and make new store -> shapefile, name and description as “shp” and shapefile location as the downloaded shapefile.
⦁	There are json files in TerriaJS folder like “composite.json”, there I have to add the catalog item pointing to my geoserver instance in the following format:
{
  "type": "wms",
  "name": "My GeoServer WMS",
  "url": "http://localhost:8082/geoserver/wms",
  "layers": "your_layer_name",
  "parameters": {
    "transparent": true,
    "format": "image/png"
  }
}
⦁	Open TerriaJS instance in browser and verify If the layer is available

___________________________________________________________________________

03/06
-> wanted to research about shp files and publishing it in geoserver, turns out the issue of network and software download exists and wasn't able to catch up on most of that